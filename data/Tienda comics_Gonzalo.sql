﻿DROP DATABASE IF EXISTS tienda_comics;
CREATE DATABASE tienda_comics;

USE tienda_comics;

CREATE OR REPLACE TABLE establecimientos(
    codigo int (5),
    nombre varchar(20),
    lugar varchar(20),
    PRIMARY KEY (codigo)
  );

CREATE OR REPLACE TABLE telefonos(
    codigo_establecimiento int (5),
    telefonos varchar(12),
    PRIMARY KEY (codigo_establecimiento,telefonos)
  );



CREATE OR REPLACE TABLE vendedores(
    dni varchar(9),
    codigo_establecimiento int(5),
    nombre varchar(20),
    PRIMARY KEY (dni)
  );

CREATE OR REPLACE TABLE dibujantes(
    codigo int(5),
    nombre varchar(20),
    apellido1 varchar(20),
    apellido2 varchar(20),
    PRIMARY KEY (codigo)
  );

CREATE OR REPLACE TABLE editoriales(
    codigo int(5),
    nombre varchar(20),
    PRIMARY KEY (codigo)
  );

CREATE OR REPLACE TABLE clientes(
    dni varchar(9),
    nombre varchar(20),
    teléfono varchar(12),
    PRIMARY KEY (dni)
  );

CREATE OR REPLACE TABLE comics(
    codigo_numerico int(3),
    nombre varchar(100),
    coleccion varchar(100),
    n_dibujante int(3),
    codigo_dibujante int(5),
    codigo_editorial int(5),
    portada varchar (1000),
    descripción varchar(10000),
    leído boolean,
    favorito boolean,
    PRIMARY KEY (codigo_numerico,n_dibujante)
  );

CREATE OR REPLACE TABLE compras(
    id int (3),
    dni_cliente varchar(9),
    codigo_numerico_comic int(3),
    PRIMARY KEY (id)
  );

CREATE OR REPLACE TABLE ventas(
    id int (3),
    dni_vendedor varchar(9),
    codigo_numerico_comic int(3),
    PRIMARY KEY (id)
  );

ALTER TABLE telefonos
  ADD CONSTRAINT fk_telefonos_establecimientos
  FOREIGN KEY (codigo_establecimiento)
  REFERENCES establecimientos(codigo);

ALTER TABLE vendedores
  ADD CONSTRAINT fk_vendedores_establecimientos
  FOREIGN KEY (codigo_establecimiento)
  REFERENCES establecimientos(codigo);

ALTER TABLE comics
  ADD CONSTRAINT fk_comics_dibujantes
  FOREIGN KEY (codigo_dibujante)
  REFERENCES dibujantes(codigo), 

 ADD CONSTRAINT fk_comics_editoriales
  FOREIGN KEY (codigo_editorial)
  REFERENCES editoriales(codigo); 

ALTER TABLE compras
  ADD CONSTRAINT fk_compras_clientes
  FOREIGN KEY (dni_cliente)
  REFERENCES clientes(dni),


  ADD CONSTRAINT fk_compran_comics
  FOREIGN KEY (codigo_numerico_comic)
  REFERENCES comics(codigo_numerico),

  ADD CONSTRAINT uk_cliente_comic
  UNIQUE KEY (dni_cliente,codigo_numerico_comic);

ALTER TABLE ventas
  ADD CONSTRAINT fk_ventas_vendedores
  FOREIGN KEY (dni_vendedor)
  REFERENCES vendedores(dni),

  ADD CONSTRAINT fk_ventas_comics
  FOREIGN KEY (codigo_numerico_comic)
  REFERENCES comics(codigo_numerico);

INSERT INTO establecimientos(codigo,nombre,lugar)
  VALUES (1,'Nemesis','Santander'),
  (2,'Nexus','Santander'),
  (3,'Distrito Cero','Santander'),
  (4,'Akira Comics','Madrid');

INSERT INTO telefonos(codigo_establecimiento,telefonos)
  VALUES (1,'+34942056789'),
  (2,'+34942057656'),
  (3,'+34942034561'),
  (4,'+34902056789');

INSERT INTO vendedores(dni,codigo_establecimiento,nombre)
  VALUES ('72196759B',1,'Pablo Gonzalez'),
  ('72199959J',1,'Sergio Gutierrez'),
  ('72196685K',1,'Ismael Fernandez'),
  ('72193409L',1,'Carlos Lopez'),
  ('72199659Y',2,'Gonzalo Martinez'),
  ('72192385K',2,'Fernando Martín'),
  ('72191209L',2,'Guillermo Cobos'),
  ('72679959J',2,'Pedro Pérez'),
  ('72114359O',3,'Silvia Moreno'),
  ('72592385N',3,'María Echevarría'),
  ('72191275L',3,'Johan Cuevas'),
  ('72679934V',3,'Lorena Garcia'),
  ('72199612P',4,'Silvia Moreno'),
  ('72192243D',4,'María Echevarría'),
  ('72191561S',4,'Johan Cuevas'),
  ('75678759Z',4,'Lorena Garcia');

INSERT INTO dibujantes(codigo, nombre, apellido1, apellido2)
  VALUES (1,'Dan','Slott',' '),
  (2,'Jack', 'Kirby',' '),
  (3,'David', 'Aja',' '),
  (4,'Todd', 'McFarlane',' ');


INSERT INTO editoriales(codigo,nombre)
  VALUES (1,'Marvel'),
  (2,'DC'),
  (3,'IDW'),
  (4,'Image Comics');

INSERT INTO clientes(dni,nombre,teléfono)
  VALUES ('72196759B','Pablo Gonzalez','+34654789992'),
  ('78955759R','Lucas Coppola','+34654789992'),
  ('73194959B','Tomás Sánchez','+56654789912'),
  ('72196759U','Sergio Antúnez','+49654789992'),
  ('72111759A','Beatriz García','+49657787992'),
  ('73396359I','Sandra Pérez','+34654789992'),
  ('72195669K','Lara Moreno','+34654789867');

INSERT INTO comics(codigo_numerico,nombre,coleccion,codigo_dibujante,codigo_editorial,portada,descripción,leído,favorito)
  VALUES (1,'SPAWN','EL INFIERNO EN LA TIERRA',4,4,'','Asesinado por sus propios hombres, el agente gubernamental Al Simmons llegó a un trato con el diablo que lo resucitó de las profundidades del Infierno.Desde que regresó a la Tierra vigila los olvidados callejones de Nueva York como el guerrero Spawn, un héroe diferente a los demás.',TRUE,TRUE);
 


INSERT INTO compras(id,dni_cliente,codigo_numerico_comic)
  VALUES (1,'78955759R',1);

INSERT INTO ventas(id,dni_vendedor,codigo_numerico_comic)
  VALUES (1,'72199959J',1);


  



