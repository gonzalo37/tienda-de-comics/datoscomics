<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = $model->codigo_numerico;
$this->params['breadcrumbs'][] = ['label' => 'Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comics-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_numerico' => $model->codigo_numerico, 'n_dibujante' => $model->n_dibujante], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_numerico' => $model->codigo_numerico, 'n_dibujante' => $model->n_dibujante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_numerico',
            'nombre',
            'coleccion',
            'n_dibujante',
            'codigo_dibujante',
            'codigo_editorial',
            'portada',
            'descripción',
            'leído',
            'favorito',
        ],
    ]) ?>

</div>
