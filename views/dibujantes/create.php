<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujantes */

$this->title = 'Create Dibujantes';
$this->params['breadcrumbs'][] = ['label' => 'Dibujantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dibujantes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
