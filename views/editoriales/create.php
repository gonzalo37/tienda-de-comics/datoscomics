<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editoriales */

$this->title = 'Create Editoriales';
$this->params['breadcrumbs'][] = ['label' => 'Editoriales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="editoriales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
