<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "establecimientos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $lugar
 *
 * @property Telefonos[] $telefonos
 * @property Vendedores[] $vendedores
 */
class Establecimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'establecimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre', 'lugar'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'lugar' => 'Lugar',
        ];
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['codigo_establecimiento' => 'codigo']);
    }

    /**
     * Gets query for [[Vendedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendedores()
    {
        return $this->hasMany(Vendedores::className(), ['codigo_establecimiento' => 'codigo']);
    }
}
