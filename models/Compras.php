<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $id
 * @property string|null $dni_cliente
 * @property int|null $codigo_numerico_comic
 *
 * @property Comics $codigoNumericoComic
 * @property Clientes $dniCliente
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_numerico_comic'], 'integer'],
            [['dni_cliente'], 'string', 'max' => 9],
            [['dni_cliente', 'codigo_numerico_comic'], 'unique', 'targetAttribute' => ['dni_cliente', 'codigo_numerico_comic']],
            [['id'], 'unique'],
            [['codigo_numerico_comic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['codigo_numerico_comic' => 'codigo_numerico']],
            [['dni_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['dni_cliente' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_cliente' => 'Dni Cliente',
            'codigo_numerico_comic' => 'Codigo Numerico Comic',
        ];
    }

    /**
     * Gets query for [[CodigoNumericoComic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComic()
    {
        return $this->hasOne(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic']);
    }

    /**
     * Gets query for [[DniCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCliente()
    {
        return $this->hasOne(Clientes::className(), ['dni' => 'dni_cliente']);
    }
}
