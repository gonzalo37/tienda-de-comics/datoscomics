<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property string|null $dni_vendedor
 * @property int|null $codigo_numerico_comic
 *
 * @property Comics $codigoNumericoComic
 * @property Vendedores $dniVendedor
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_numerico_comic'], 'integer'],
            [['dni_vendedor'], 'string', 'max' => 9],
            [['id'], 'unique'],
            [['codigo_numerico_comic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['codigo_numerico_comic' => 'codigo_numerico']],
            [['dni_vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedores::className(), 'targetAttribute' => ['dni_vendedor' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_vendedor' => 'Dni Vendedor',
            'codigo_numerico_comic' => 'Codigo Numerico Comic',
        ];
    }

    /**
     * Gets query for [[CodigoNumericoComic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComic()
    {
        return $this->hasOne(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic']);
    }

    /**
     * Gets query for [[DniVendedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniVendedor()
    {
        return $this->hasOne(Vendedores::className(), ['dni' => 'dni_vendedor']);
    }
}
