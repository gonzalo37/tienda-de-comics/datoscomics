<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $teléfono
 *
 * @property Compras[] $compras
 * @property Comics[] $codigoNumericoComics
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['teléfono'], 'string', 'max' => 12],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'teléfono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[CodigoNumericoComics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic'])->viaTable('compras', ['dni_cliente' => 'dni']);
    }
}
