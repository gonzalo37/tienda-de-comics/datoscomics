<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedores".
 *
 * @property string $dni
 * @property int|null $codigo_establecimiento
 * @property string|null $nombre
 *
 * @property Establecimientos $codigoEstablecimiento
 * @property Ventas[] $ventas
 */
class Vendedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['codigo_establecimiento'], 'integer'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['dni'], 'unique'],
            [['codigo_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimientos::className(), 'targetAttribute' => ['codigo_establecimiento' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'codigo_establecimiento' => 'Codigo Establecimiento',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[CodigoEstablecimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEstablecimiento()
    {
        return $this->hasOne(Establecimientos::className(), ['codigo' => 'codigo_establecimiento']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['dni_vendedor' => 'dni']);
    }
}
