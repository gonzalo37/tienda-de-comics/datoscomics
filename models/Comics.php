<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comics".
 *
 * @property int $codigo_numerico
 * @property string|null $nombre
 * @property string|null $coleccion
 * @property int $n_dibujante
 * @property int|null $codigo_dibujante
 * @property int|null $codigo_editorial
 * @property string|null $portada
 * @property string|null $descripción
 * @property int|null $leído
 * @property int|null $favorito
 *
 * @property Dibujantes $codigoDibujante
 * @property Editoriales $codigoEditorial
 * @property Compras[] $compras
 * @property Clientes[] $dniClientes
 * @property Ventas[] $ventas
 */
class Comics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_numerico', 'n_dibujante'], 'required'],
            [['codigo_numerico', 'n_dibujante', 'codigo_dibujante', 'codigo_editorial', 'leído', 'favorito'], 'integer'],
            [['nombre', 'coleccion'], 'string', 'max' => 100],
            [['portada'], 'string', 'max' => 1000],
            [['descripción'], 'string', 'max' => 10000],
            [['codigo_numerico', 'n_dibujante'], 'unique', 'targetAttribute' => ['codigo_numerico', 'n_dibujante']],
            [['codigo_dibujante'], 'exist', 'skipOnError' => true, 'targetClass' => Dibujantes::className(), 'targetAttribute' => ['codigo_dibujante' => 'codigo']],
            [['codigo_editorial'], 'exist', 'skipOnError' => true, 'targetClass' => Editoriales::className(), 'targetAttribute' => ['codigo_editorial' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_numerico' => 'Codigo Numerico',
            'nombre' => 'Nombre',
            'coleccion' => 'Coleccion',
            'n_dibujante' => 'N Dibujante',
            'codigo_dibujante' => 'Codigo Dibujante',
            'codigo_editorial' => 'Codigo Editorial',
            'portada' => 'Portada',
            'descripción' => 'Descripción',
            'leído' => 'Leído',
            'favorito' => 'Favorito',
        ];
    }

    /**
     * Gets query for [[CodigoDibujante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDibujante()
    {
        return $this->hasOne(Dibujantes::className(), ['codigo' => 'codigo_dibujante']);
    }

    /**
     * Gets query for [[CodigoEditorial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEditorial()
    {
        return $this->hasOne(Editoriales::className(), ['codigo' => 'codigo_editorial']);
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[DniClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniClientes()
    {
        return $this->hasMany(Clientes::className(), ['dni' => 'dni_cliente'])->viaTable('compras', ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['codigo_numerico_comic' => 'codigo_numerico']);
    }
}
