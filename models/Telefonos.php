<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $codigo_establecimiento
 * @property string $telefonos
 *
 * @property Establecimientos $codigoEstablecimiento
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_establecimiento', 'telefonos'], 'required'],
            [['codigo_establecimiento'], 'integer'],
            [['telefonos'], 'string', 'max' => 12],
            [['codigo_establecimiento', 'telefonos'], 'unique', 'targetAttribute' => ['codigo_establecimiento', 'telefonos']],
            [['codigo_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimientos::className(), 'targetAttribute' => ['codigo_establecimiento' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_establecimiento' => 'Codigo Establecimiento',
            'telefonos' => 'Telefonos',
        ];
    }

    /**
     * Gets query for [[CodigoEstablecimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEstablecimiento()
    {
        return $this->hasOne(Establecimientos::className(), ['codigo' => 'codigo_establecimiento']);
    }
}
